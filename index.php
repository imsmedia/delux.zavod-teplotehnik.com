<?php session_start();
$site_url = 'http://zavod-teplotehnik.com/lp/kotli_delux/';
if (array_key_exists('utm_referrer', $_GET)) {
  if (strpos($site_url, $_GET['utm_referrer'])===false) {
    $_SESSION['referer'] = $_GET['utm_referrer'];
  }
}
if (array_key_exists('utm_source', $_GET)) {
  if (strpos($site_url, $_GET['utm_source'])===false) {
    $_SESSION['sourse'] = $_GET['utm_source'];
  }
  if (strpos($site_url, $_GET['utm_term'])===false) {
    $_SESSION['term'] = $_GET['utm_term'];
  }
  if (strpos($site_url, $_GET['utm_campaign'])===false) {
    $_SESSION['campaign'] = $_GET['utm_campaign'];
  }
}
?>
<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">

<head>
 <!-- Google Analytics Content Experiment code -->
<!--  <script>function utmx_section(){}function utmx(){}(function(){var-->
<!--          k='117568374-0',d=document,l=d.location,c=d.cookie;-->
<!--    if(l.search.indexOf('utm_expid='+k)>0)return;-->
<!--    function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.-->
<!--    indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.-->
<!--            length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(-->
<!--            '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':-->
<!--                    '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+-->
<!--            '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().-->
<!--            valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+-->
<!--            '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();-->
<!--  </script><script>utmx('url','A/B');</script>-->
 <!-- End of Google Analytics Content Experiment code -->
  <meta charset="UTF-8">
  <title>Буржуй</title>
  <meta name="description" content="твердотопливные котлы от производителя">
  <meta name="keywords" content="">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="js/libs/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/owl.carousel.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/flipclock.css">
  <link rel="stylesheet" href="js/libs/fancybox/jquery.fancybox.css">
  <meta name="viewport" content="width=1200">
  <script src="js/libs/jquery.min.js"></script>
  <script src="js/libs/jquery-ui/jquery-ui.min.js"></script>
  <script src="js/libs/maskedinput.min.js"></script>
  <script src="js/libs/validate/jquery.validate.min.js"></script>
  <script src="js/libs/fancybox/jquery.fancybox.pack.js"></script>
  <script src="js/tabs.js"></script>
  <script src="js/libs/owl.carousel.js"></script>
  <script src="js/libs/jquery.reel.js"></script>
<!--  <script src="js/libs/flipclock.js"></script>-->
  <script src="js/libs/slick.min.js"></script>
<!--  <script type="text/javascript">-->
<!--    var clock;-->
<!--    $(document).ready(function() {-->
<!--      var currentDate = new Date();-->
<!--      var pastDate  = new Date(2016, currentDate.getMonth(), currentDate.getDate()+1);-->
<!--      var diff =  pastDate.getTime() / 1000 - currentDate.getTime() / 1000;-->
<!--      clock = $('#timer10').FlipClock(diff, {-->
<!--        clockFace: 'HourlyCounter',-->
<!--        showSeconds: true,-->
<!--        countdown: true,-->
<!--        language: 'ru'-->
<!--      });-->
<!--      clock = $('#timer12').FlipClock(diff, {-->
<!--        clockFace: 'HourlyCounter',-->
<!--        showSeconds: true,-->
<!--        countdown: true,-->
<!--        language: 'ru'-->
<!--      });-->
<!--      clock = $('#timer15').FlipClock(diff, {-->
<!--        clockFace: 'HourlyCounter',-->
<!--        showSeconds: true,-->
<!--        countdown: true,-->
<!--        language: 'ru'-->
<!--      });-->
<!--      clock = $('#timer18').FlipClock(diff, {-->
<!--        clockFace: 'HourlyCounter',-->
<!--        showSeconds: true,-->
<!--        countdown: true,-->
<!--        language: 'ru'-->
<!--      });-->
<!--      clock = $('#timer20').FlipClock(diff, {-->
<!--        clockFace: 'HourlyCounter',-->
<!--        showSeconds: true,-->
<!--        countdown: true,-->
<!--        language: 'ru'-->
<!--      });-->
<!--    });-->
<!--  </script>-->
</head>

<body>
<script>
  dataLayer = [];
</script>
<!-- Google Tag Manager -->
<!--<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MLRTGW"-->
<!--                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>-->
<!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
<!--      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
<!--      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
<!--      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
<!--  })(window,document,'script','dataLayer','GTM-MLRTGW');</script>-->
<!-- End Google Tag Manager -->
  <section class="wrapper scr-i__wrapper">
    <header class="inner header">
      <img class="header__logo" src="img/logo.png" alt="Буржуй">
<!--      <div class="header__title">-->
<!--        завод<br>твердотопливных котлов-->
<!--      </div>-->
      <div class="header__buttonbox" tabindex="1">
        <div class="header__buttonbox__button" data-input="Обратный звонок" data-google="/callback.html" data-init="modal" data-modal="#callback-modal2">
          Обратный звонок
        </div>
      </div>
      <span class="header__phone">
        Работаем без выходных
        <a class="header__link" href="tel:+380667893083">+38 (097) 272-81-82</a>
        <a class="header__link" href="tel:+380975896160">+38 (050) 020-99-07</a>
      </span>
    </header>
    <div class="inner scr-i">
       <span class="scr-i__title">
       Твердотопливные котлы длительного горения
      </span>
        <span class="scr-i__subtitle">
       до 48 часов на одной загрузке
      </span>
      <div class="scr-i__form-wrapper">
        <div class="scr-i__form-content">
          <span class="scr-i__form-title">
            Подберите котел за 5 минут
          </span>
          <span class="scr-i__form-subtitle">
            Под ваше помещение
          </span>
          <div class="scr-i__form">
            <form class="formGo" action="sendmessage.php" method="POST">
              <div class="scr-i__input-wrapper">
                <input type="text" class="scr-i__input" placeholder="Площадь помещения"   name="area" >
              </div>
              <div class="scr-i__input-wrapper">
                <input type="text"  class="scr-i__input" name="phone" placeholder="Телефон">
                <input type="hidden" value="/first-form.html" name="htmlData">
                <input type="hidden" value="Подобрать котел за 5 мин" name="order">
              </div>
              <div class="scr-i__input-wrapper--wide">
                <button class="scr-i__button" type="submit" data-content="Подобрать"></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <span class="scr-i__deliv">
         Доставляем по всей Украине за 2 дня
      </span>
      <img class="scr-i__scroll-to" src="img/arrow-down.png" alt="">
    </div>
  </section>

<!--<section class="scr-sale__wrapper wrapper">-->
<!--  <div class="scr-sale__inner inner">-->
<!--    <div class="scr-sale__box">-->
<!--      <span class="scr-sale__title">-->
<!--        Акция!-->
<!--      </span>-->
<!--      <div class="button" data-content="Учавствовать в акции" data-input="Учавствовать в акции"  data-init="modal" data-google="/sale.html"  data-modal="#callback-modal"></div>-->
<!--    </div>-->
<!--  </div>-->
<!--</section>-->

  <section class="scr-iii__wrapper wrapper">
    <div class="scr-iii__inner inner">
      <span class="scr-iii__title">
        Популярные модели котлов
      </span>
      <div id="tabs" class="tabs">
        <img src="img/3pieces.png" class="guarantee" alt="">
        <div class="tabs-nav">
          <ul>
            <li class="tabs-nav__item" data-content="Deluxe-10">Deluxe-10</li>
            <li class="tabs-nav__item" data-content="Deluxe-14">Deluxe-14</li>
            <li class="tabs-nav__item" data-content="Deluxe-18">Deluxe-18</li>
            <li class="tabs-nav__item" data-content="Deluxe-24">Deluxe-24</li>
          </ul>
        </div>
        <div class="tabs-content">
          <ul>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img class="imageKotel10" src="img/del10.jpg" width="500" height="375" />
                  </div>
                </div>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="declaration" >
                   Параметры:
                  </div>
                  <div class="line">
                    <span>Мощность:</span><span>10 кВт</span>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь:</span><span>100м2</span>
                  </div>
                  <div class="line">
                    <span>КПД:</span><span>>76%</span>
                  </div>
                  <div class="line">
                    <span>Объем топки:</span><span>56 дм3</span>
                  </div>
                  <div class="line">
                    <span>Частота загрузок топлива</span><span>1раз в сутки</span>
                  </div>
                  <a class="specification-button fancybox" href="#delux10">Подробное описание</a>
                </div>
                <div class="salebox">
                  <div class="titlesale">
                    На данный товар<br> действует<br> скидка 20%
                  </div>
                  <div class="box">
                    <div class="lastprice">15 640 грн.
                    </div>
                    <div class="newprice">
                      13 220 грн.
                    </div>
                    <div class="button" data-content="Купить по акции" data-input="Заказать по акции Deluxe-10"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                    <div class="lastPs">
                      Осталось 3 штуки
                    </div>
                  </div>
                </div>
              </div>
              <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                <p class="ask">
                  Задайте вопрос по этому товару
                </p>
                <div class="tabs-content__input-wrapper">
                  <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите телефон">
                </div>
                <div class="tabs-content__input-wrapper">
                  <button type="submit" class="tabs-content__button" data-content="Спросить"></button>
                </div>
                <input type="hidden" value="Deluxe-10 задать вопрос" name="order">
                <input type="hidden" value="/vopros.html" name="htmlData">
              </form>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img class="imageKotel10" src="img/del10.jpg" width="500" height="375" />
                  </div>
                </div>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="declaration" >
                    Параметры:
                  </div>
                  <div class="line">
                    <span>Мощность:</span><span>14 кВт</span>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь:</span><span>140м2</span>
                  </div>
                  <div class="line">
                    <span>КПД:</span><span>>76%</span>
                  </div>
                  <div class="line">
                    <span>Объем топки:</span><span>67 дм3</span>
                  </div>
                  <div class="line">
                    <span>Частота загрузок топлива</span><span>1раз в сутки</span>
                  </div>
                  <a class="specification-button fancybox" href="#delux14">Подробное описание</a>
                </div>
                <div class="salebox">
                  <div class="titlesale">
                    На данный товар<br> действует<br> скидка 20%
                  </div>
                  <div class="box">
                    <div class="lastprice">15 640 грн.
                    </div>
                    <div class="newprice">
                      13 220 грн.
                    </div>
                    <div class="button" data-content="Купить по акции" data-input="Заказать по акции Deluxe-14"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                    <div class="lastPs">
                      Осталось 2 штуки
                    </div>
                  </div>
                </div>
              </div>
              <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                <p class="ask">
                  Задайте вопрос по этому товару
                </p>
                <div class="tabs-content__input-wrapper">
                  <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите телефон">
                </div>
                <div class="tabs-content__input-wrapper">
                  <button type="submit" class="tabs-content__button" data-content="Спросить"></button>
                </div>
                <input type="hidden" value="Deluxe-14 задать вопрос" name="order">
                <input type="hidden" value="/vopros.html" name="htmlData">
              </form>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img class="imageKotel10" src="img/del10.jpg" width="500" height="375" />
                  </div>
                </div>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="declaration" >
                    Параметры:
                  </div>
                  <div class="line">
                    <span>Мощность:</span><span>18 кВт</span>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь:</span><span>180м2</span>
                  </div>
                  <div class="line">
                    <span>КПД:</span><span>>76%</span>
                  </div>
                  <div class="line">
                    <span>Объем топки:</span><span>77 дм3</span>
                  </div>
                  <div class="line">
                    <span>Частота загрузок топлива</span><span>1раз в сутки</span>
                  </div>
                  <a class="specification-button fancybox" href="#delux18">Подробное описание</a>
                </div>
                <div class="salebox">
                  <div class="titlesale">
                    На данный товар<br> действует<br> скидка 20%
                  </div>
                  <div class="box">
                    <div class="lastprice">15 640 грн.
                    </div>
                    <div class="newprice">
                      13 220 грн.
                    </div>
                    <div class="button" data-content="Купить по акции" data-input="Заказать по акции Deluxe-18"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                    <div class="lastPs">
                      Осталось 4 штуки
                    </div>
                  </div>
                </div>
              </div>
              <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                <p class="ask">
                  Задайте вопрос по этому товару
                </p>
                <div class="tabs-content__input-wrapper">
                  <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите телефон">
                </div>
                <div class="tabs-content__input-wrapper">
                  <button type="submit" class="tabs-content__button" data-content="Спросить"></button>
                </div>
                <input type="hidden" value="Deluxe-18 задать вопрос" name="order">
                <input type="hidden" value="/vopros.html" name="htmlData">
              </form>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img class="imageKotel10" src="img/del10.jpg" width="500" height="375" />
                  </div>
                </div>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="declaration" >
                    Параметры:
                  </div>
                  <div class="line">
                    <span>Мощность:</span><span>24 кВт</span>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь:</span><span>240м2</span>
                  </div>
                  <div class="line">
                    <span>КПД:</span><span>>76%</span>
                  </div>
                  <div class="line">
                    <span>Объем топки:</span><span>100 дм3</span>
                  </div>
                  <div class="line">
                    <span>Частота загрузок топлива</span><span>1раз в сутки</span>
                  </div>
                  <a class="specification-button fancybox" href="#delux24">Подробное описание</a>
                </div>
                <div class="salebox">
                  <div class="titlesale">
                    На данный товар<br> действует<br> скидка 20%
                  </div>
                  <div class="box">
                    <div class="lastprice">15 640 грн.
                    </div>
                    <div class="newprice">
                      13 220 грн.
                    </div>
                    <div class="button" data-content="Купить по акции" data-input="Заказать по акции Deluxe-24"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                    <div class="lastPs">
                      Осталось 2 штуки
                    </div>
                  </div>
                </div>
              </div>
              <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                <p class="ask">
                  Задайте вопрос по этому товару
                </p>
                <div class="tabs-content__input-wrapper">
                  <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите телефон">
                </div>
                <div class="tabs-content__input-wrapper">
                  <button type="submit" class="tabs-content__button" data-content="Спросить"></button>
                </div>
                <input type="hidden" value="Deluxe-24 задать вопрос" name="order">
                <input type="hidden" value="/vopros.html" name="htmlData">
              </form>
            </li>

          </ul>
        </div>

      </div>
    </div>
  </section>

<section class="scr-ix__wrapper wrapper">
  <div class="scr-ix__inner inner">
    <div class="scr-ix__title">
      Почему котлы Буржуй "DeLuxe" лидеры<br>
      по продажам в Украине
    </div>
    <div class="scr-ix__col">
      <div class="scr-ix__col--fiches">Автономность</div>
      <div class="scr-ix__col--descr">До 48 часов на одной<br> загрузке твердого топлива</div>
      <div class="scr-ix__col--fiches">Надежность</div>
      <div class="scr-ix__col--descr">Толщина стали 3мм, все<br>узлы доступны для<br> обслуживания</div>
      <div class="scr-ix__col--fiches">Долговечность</div>
      <div class="scr-ix__col--descr">Расчетный срок службы не<br> менее 10 лет</div>
      <div class="scr-ix__col--fiches none">Эффективность</div>
      <div class="scr-ix__col--descr">КПД от 80 до 90%</div>
    </div>
    <div class="scr-ix__col2">
      <div class="scr-ix__col--fiches">Установлена система</div>
      <div class="scr-ix__col--descr">автоматической поддержки<br> температуры</div>
      <div class="scr-ix__col--fiches">Работает</div>
      <div class="scr-ix__col--descr">на всех видах твердого<br> топлива</div>
      <div class="scr-ix__col--fiches">Высокое качество</div>
      <div class="scr-ix__col--descr">покраски облицовочных<br> панелей и корпуса котла</div>
      <div class="scr-ix__col--fiches none">Наличие камеры</div>
      <div class="scr-ix__col--descr">сжигания пиролизных газов</div>

    </div>
  </div>
</section>





<!--<section class="scr-boxes__wrapper wrapper">-->
<!--  <div class="inner">-->
<!--    <div class="scr-boxes__box">-->
<!--      <div class="scr-boxes__box--text">-->
<!--        До 8 часов<br/>на одной<br/>закладке топлива-->
<!--      </div>-->
<!--    </div>-->
<!--    <div class="scr-boxes__box">-->
<!--      <div class="scr-boxes__box--text">-->
<!--        Высокое качество  <br/>материалов  <br/>и сборки-->
<!--      </div>-->
<!--    </div>-->
<!--    <div class="scr-boxes__box">-->
<!--      <div class="scr-boxes__box--text">-->
<!--        Котел «Буржуй»  <br/>сертифицирован  <br/>и соответствует всем  <br/>условиям эксплуатации-->
<!--      </div>-->
<!--    </div>-->
<!--    <div class="scr-boxes__box">-->
<!--      <div class="scr-boxes__box--text">-->
<!--        Всего 1 обращение  <br/>по гарантии  <br/>из 4000  <br/>проданных котлов-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</section>-->

<!--  <section class="scr-vi__wrapper wrapper">-->
<!--    <div class="scr-vi__inner inner">-->
<!--      <h2 class="scr-vi__fiches">-->
<!--        Всего <span style="color: #00e4ff;font-size:35px;">1 клиент</span> обратился по <span style="color: #00e4ff;font-size:27px;">гарантии</span></br>-->
<!--        из <span style="color: #00e4ff;font-size:38px;">4000</span> проданных котлов-->
<!--      </h2>-->
<!--      <h2 class="scr-vi__fiches">-->
<!--        Дверца у наших котлов<br>-->
<!--        открывается <span style="color: #00e4ff;font-size:28px;">во все стороны</span>-->
<!--      </h2>-->
<!--      <h2 class="scr-vi__fiches">-->
<!--        Котел «Буржуй» является </br>-->
<!--        <span style="color: #00e4ff;text-decoration: underline;font-size:33px;">сертифицированными</span><br> и соответствует всем условиям-->
<!--      </h2>-->
<!--      <h2 class="scr-vi__fiches">-->
<!--        Мы добиваемся такого</br>-->
<!--        качества так как <span style="color: #00e4ff; font-size: 28px;">проверяем</span> котлы</br>-->
<!--        на каждом этапе производства <span style="color: #ffd800; font-size: 35px;">дважды</span>-->
<!--      </h2>-->
<!--      <div class="scr-vi__playbutton fancybox fancybox.iframe" href="https://www.youtube.com/embed/s0bH2JPrUq0?autoplay=true">-->
<!--        <img src="img/play.png" alt="">-->
<!--        <p>Посмотреть видео</p>-->
<!--      </div>-->
<!--    </div>-->
<!--  </section>-->


<!--<section class="scr_video">-->
<!--  <div class="scr_video__box fancybox fancybox.iframe" href="https://www.youtube.com/embed/s0bH2JPrUq0?autoplay=true">-->
<!--    <svg version="1.1" id="_x31_" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"-->
<!--         y="0px"-->
<!--         viewBox="542.5 391.8 20.7 20.7" enable-background="new 542.5 391.8 20.7 20.7" xml:space="preserve">-->
<!--      <path fill="none" stroke="#FFFFFF" stroke-width="0.8" stroke-miterlimit="22.9256" d="M558.6,410.2c-1.7,1.2-3.7,1.8-5.7,1.8-->
<!--	c-5.5,0-9.8-4.4-9.8-9.8s4.4-9.8,9.8-9.8s9.8,4.4,9.8,9.8c0,0,0,0.1,0,0.1"/>-->
<!--      <path fill="none" stroke="#FFFFFF" stroke-width="0.8" stroke-linecap="round" stroke-linejoin="round"-->
<!--            stroke-miterlimit="500" d="-->
<!--	M551.3,406.6v-8.9l4.5,4.5L551.3,406.6L551.3,406.6z"/>-->
<!--</svg>-->
<!--    <p>Посмотреть видео</p>-->
<!--  </div>-->
<!--</section>-->
<!---->
<!---->
<!---->
<!--    <section class="scr-egg__wrapper wrapper">-->
<!--        <div class="scr-egg__inner inner">-->
<!--            <div class="scr-egg__item">-->
<!--                <div class="scr-egg__item--img"></div>-->
<!--                <div class="scr-egg__item--text">Украинский<br> производитель</div>-->
<!--            </div>-->
<!--            <div class="scr-egg__item">-->
<!--                <div class="scr-egg__item--img"></div>-->
<!--                <div class="scr-egg__item--text">Лучшая цена<br> без посредников</div>-->
<!--            </div>-->
<!--            <div class="scr-egg__item">-->
<!--                <div class="scr-egg__item--img"></div>-->
<!--                <div class="scr-egg__item--text">Все товары<br> на складе</div>-->
<!--            </div>-->
<!--            <div class="scr-egg__item">-->
<!--                <div class="scr-egg__item--img"></div>-->
<!--                <div class="scr-egg__item--text">Доставка и установка<br> по всей Украине</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!---->
<!--<section class="scr-production__wrapper wrapper">-->
<!--  <div class="scr-production__inner inner">-->
<!--    <div class="scr-production__title">-->
<!--      Мы производим более 1000 котлов в год-->
<!--    </div>-->
<!--    <div class="scr-production--slider">-->
<!--      <div class="item">-->
<!--        <img src="img/prod-stok/sp02.jpg" alt="">-->
<!--      </div>-->
<!--      <div class="item">-->
<!--        <img src="img/prod-stok/sp03.jpg" alt="">-->
<!--      </div>-->
<!--      <div class="item">-->
<!--        <img src="img/prod-stok/sp07.jpg" alt="">-->
<!--      </div>-->
<!--      <div class="item">-->
<!--        <img src="img/prod-stok/sp02.jpg" alt="">-->
<!--      </div>-->
<!--      <div class="item">-->
<!--        <img src="img/prod-stok/sp03.jpg" alt="">-->
<!--      </div>-->
<!--      <div class="item">-->
<!--        <img src="img/prod-stok/sp07.jpg" alt="">-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</section>-->
<!---->
<!--<section class="scr-deliv__wrapper wrapper">-->
<!--  <div class="scr-deliv__inner inner">-->
<!--    <div class="scr-deliv__title">-->
<!--      Котлы буржуй доставляются во все регионы Украины-->
<!--    </div>-->
<!--    <div class="scr-deliv__subtitle">-->
<!--      Срок доставки - до 3 дней-->
<!--    </div>-->
<!--    <img src="img/map_ukr.png" class="scr-deliv__map">-->
<!--  </div>-->
<!--</section>-->
<!---->
<!--<section class="scr-fake__wrapper wrapper">-->
<!--  <div class="scr-fake__inner inner">-->
<!--    <div class="scr-fake__title">-->
<!--      Внимание! Остерегайтесь подделок!-->
<!--    </div>-->
<!--    <div class="scr-fake__descr">-->
<!--      Высокая популярность котлов "Буржуй" привела к появлению на рынке<br> большого количества контрафактной продукции по более низкой цене, но не<br> соответствующей стандартам качества нашей компании.-->
<!--    </div>-->
<!--    <div class="scr-fake__subtitle">-->
<!--      Покупайте оригинальные котлы только у официальных дилеров!-->
<!--    </div>-->
<!--  </div>-->
<!--</section>-->
<!---->
<!--<section class="scr-diller__wrapper wrapper">-->
<!--  <div class="scr-diller__inner inner">-->
<!--    <div class="scr-diller__title">-->
<!--      Найдите дилера в вашем регионе-->
<!--    </div>-->
<!--    <form class="scr-diller__form-wrapper formGo" action="sendmessage.php" method="POST">-->
<!--      <div class="scr-diller__input-wrapper">-->
<!--        <select name="state" size="1" class="scr-diller__input" style="text-align-last:center;">-->
<!--          <option disabled selected="selected">Выберите область</option>-->
<!--          <option value="АР Крым">АР Крым</option>-->
<!--          <option value="Винницкая обл.">Винницкая обл.</option>-->
<!--          <option value="Волынская обл.">Волынская обл.</option>-->
<!--          <option value="Днепропетровская обл.">Днепропетровская обл.</option>-->
<!--          <option value="Донецкая обл.">Донецкая обл.</option>-->
<!--          <option value="Житомирская обл.">Житомирская обл.</option>-->
<!--          <option value="Закарпатская обл.">Закарпатская обл.</option>-->
<!--          <option value="Запорожская обл.">Запорожская обл.</option>-->
<!--          <option value="Ивано-Франковская обл.">Ивано-Франковская обл.</option>-->
<!--          <option value="Киевская обл.">Киевская обл.</option>-->
<!--          <option value="Кировоградская обл.">Кировоградская обл.</option>-->
<!--          <option value="Луганская обл.">Луганская обл.</option>-->
<!--          <option value="Львовская обл.">Львовская обл.</option>-->
<!--          <option value="Николаевская обл.">Николаевская обл.</option>-->
<!--          <option value="Одесская обл.">Одесская обл.</option>-->
<!--          <option value="Полтавская обл.">Полтавская обл.</option>-->
<!--          <option value="Ровенская обл.">Ровенская обл.</option>-->
<!--          <option value="Сумская обл.">Сумская обл.</option>-->
<!--          <option value="Тернопольская обл.">Тернопольская обл.</option>-->
<!--          <option value="Харьковская обл.">Харьковская обл.</option>-->
<!--          <option value="Херсонская обл.">Херсонская обл.</option>-->
<!--          <option value="Хмельницкая обл.">Хмельницкая обл.</option>-->
<!--          <option value="Черкасская обл.">Черкасская обл.</option>-->
<!--          <option value="Черниговская обл.">Черниговская обл.</option>-->
<!--          <option value="Черновицкая обл.">Черновицкая обл.</option>-->
<!--          <option value="Черновицкая обл.">Черновицкая обл.</option>-->
<!--        </select>-->
<!--      </div>-->
<!--      <div class="scr-diller__input-wrapper">-->
<!--        <input type="text" class="scr-diller__input" name="city" placeholder="Название населенного пункта">-->
<!--      </div>-->
<!--      <div class="scr-diller__input-wrapper">-->
<!--        <input type="text" class="scr-diller__input" name="phone" placeholder="Ваш телефон">-->
<!--      </div>-->
<!--      <input type="hidden" value="Поиск дилера" name="order">-->
<!--      <input type="hidden" value="/find-diller.html" name="htmlData">-->
<!--      <div class="scr-diller__input-wrapper">-->
<!--        <button type="submit" class="scr-diller__button" data-content="Найти дилера"></button>-->
<!--      </div>-->
<!--    </form>-->
<!--  </div>-->
<!--</section>-->
<!---->
<!--<section class="scr-reviews__wrapper wrapper">-->
<!--  <div class="scr-reviews__inner inner">-->
<!--    <div class="scr-reviews--slider">-->
<!--      <div class="scr-reviews__item">-->
<!--        <div class="scr-reviews__review-image">-->
<!--          <img src="img/vlad_y.jpg" alt="">-->
<!--          <img src="img/shadow.png" alt="">-->
<!--        </div>-->
<!--        <div class="scr-reviews__review-box">-->
<!--          <div class="scr-reviews__review-box--name">-->
<!--            Юферова Владислава Владимировна-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--place">-->
<!--            Харьковская обл., Бабаи-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--text">-->
<!--            Заказали котел Буржуй. На заводе очень внимательные и отзывчивые ребята проконсультировали все доставили в срок. После установки котла остались довольны его работай намного теплее чем при обогреве газом. Большое спасибо ребятам!-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="scr-reviews__item">-->
<!--        <div class="scr-reviews__review-image">-->
<!--          <img src="img/lachyk.jpg" alt="">-->
<!--          <img src="img/shadow.png" alt="">-->
<!--        </div>-->
<!--        <div class="scr-reviews__review-box">-->
<!--          <div class="scr-reviews__review-box--name">-->
<!--            Ляшук Александр Александрович-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--place">-->
<!--            Луганская обл, Лисичянск-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--text">-->
<!--            Котел "Буржуй" КП-12п приобрел 3 месяца назад. Котел отличный. Топлю как дровами так и углем. Топка вместительная, дрова закладываю крупно рубленые. Дом отапливает хорошо. Затапливаю 2 раза в сутки утром и вечером. Ночью не топлю, только в сильные морозы. Котел экономный, за 3 месяца окупился полностью, т.к. газовый котел, учитывая сегодняшние цены на газ, получился очень затратный. Планирую еще приобрести регулятор тяги, чтобы регулировать температуру в котле не в ручную, а автоматически. А так, в целом, котлом доволен.-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="scr-reviews__item">-->
<!--        <div class="scr-reviews__review-image">-->
<!--          <img src="img/dffd.jpg" alt="">-->
<!--          <img src="img/shadow.png" alt="">-->
<!--        </div>-->
<!--        <div class="scr-reviews__review-box">-->
<!--          <div class="scr-reviews__review-box--name">-->
<!--            Мельников Сергей Валерьевич-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--place">-->
<!--            Луганская обл, Лисичянск-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--text">-->
<!--            Котел очень хороший на свои деньги. Взял его на дом 120м2, батареи трубные, объем 220 литров, справляется на отлично с насосом температура воды при активной топки достигает практически 85-90 градусов. Топлю дровами иногда углем, горит все. В общем доволен! минусов нет. Еще хорошо то, что сразу можно воду греть.-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="scr-reviews__item">-->
<!--        <div class="scr-reviews__review-image">-->
<!--          <img src="img/Kyxar.jpg" alt="">-->
<!--          <img src="img/shadow.png" alt="">-->
<!--        </div>-->
<!--        <div class="scr-reviews__review-box">-->
<!--          <div class="scr-reviews__review-box--name">-->
<!--            Шеленгович Павлина Викторовна-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--place">-->
<!--            Черкасской обл., Христиновка-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--text">-->
<!--            Спасибо Вам , за доступные цены на котлы.  Купили котел в ноябре 2015 и он слава Богу оправдал наши надежды.-->
<!--            В сравнении с газовым отоплением доме стало намного теплее . Не дымит, когда подкладываешь дрова. Понравилось то, что есть  заслонка при выходе на дымоход , с ней  экономится много тепла. Одним словом - работа котла БУРЖУЙ мне понравилась!-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="scr-reviews__item">-->
<!--        <div class="scr-reviews__review-image fancybox fancybox.iframe" href="https://www.youtube.com/embed/s0bH2JPrUq0?autoplay=true">-->
<!--          <img src="img/tip.jpg" alt="">-->
<!--          <img src="img/shadow.png" alt="">-->
<!--        </div>-->
<!--        <div class="scr-reviews__review-box">-->
<!--          <div class="scr-reviews__review-box--name">-->
<!--            Шевченко Юрий Николаевич-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--place">-->
<!--            Луганская обл, Старобельск-->
<!--          </div>-->
<!--          <div class="scr-reviews__review-box--text">-->
<!--            Купил  котел Буржуй-15 на твердом топливе осенью. Установил перед началом морозов, так как хотелось испытать его на экономичность. Дрова загружал длиной примерно 50-60см диметром разные от 5 до 150 см в диаметре. Одной загрузки хватает на 5 часов при том что я установил на него регулятор тяги. Из плюсов котла это первое что его  цена ,второе конечно 2 конфорки что дает нагреть для хозяйства теплую воду используя тепло конфорок .Дверца  сделана хорошо прилегает по всему периметру .Из минусов то что нет в комплекте асбестового шнура под чугунную плиту с конфорками .без него во время растопки пока не разгорится немного идет дым, а когда разгорится тогда нормально. А так котел рекомендую, за такую цену котла с такими параметрами не найдете, а если и найдете то цена буде процентов на 20 выше.-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</section>-->


  <section class="scr-vii__wrapper wrapper">
    <div class="scr-vii__inner inner">
      <div class="scr-vii__title">
       Появились вопросы?
      </div>
      <form class="scr-vii__form-wrapper formGo" action="sendmessage.php" method="POST">
        <div class="scr-vii__title-form">
          Оставьте номер телефона
        </div>
        <div class="scr-vii__descr">
          И мы проконсультируем вас по всем вопросам
        </div>
        <div class="scr-vii__input-wrapper">
          <input type="text"  class="scr-vii__input" name="phone" placeholder="Номер телефона">
          <input type="hidden" value="Получить консультацию (последняя форма)" name="order">
          <input type="hidden" value="/konsult.html" name="htmlData">
        </div>
        <div class="scr-vii__input-wrapper">
          <button  type="submit" class="scr-vii__button" data-content="Получить консультацию"></button>
        </div>
      </form>
    </div>
    <footer class="inner footer">
      <span class="footer__phone">
        Работаем без выходных
        <a class="footer__link" href="tel:+380667893083">+38 (097) 272-81-82</a>
        <a class="footer__link" href="tel:+380975896160">+38 (050) 020-99-07</a>
      </span>
      <img class="footer__logo" src="img/logo-f.png" alt="Буржуй">
      <a href="//imsmedia.net.ua/" target="_blank" class="footer__logoims">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 98.5 34.9" enable-background="new 0 0 98.5 34.9" xml:space="preserve">
        <g>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#C5C6C6" d="M16.2,29.5c7.2,0,13-5.8,13-13s-5.8-13-13-13s-13,5.8-13,13
          		S9.1,29.5,16.2,29.5z"></path>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#C5C6C6" d="M12.6,0.8c0.6-0.1,1.2-0.2,1.8-0.3c0.6-0.1,1.2-0.1,1.8-0.1
          		c4.4,0,8.5,1.8,11.4,4.7c2.9,2.9,4.7,6.9,4.7,11.4c0,0.8-0.1,1.5-0.2,2.3c-0.1,0.7-0.3,1.5-0.5,2.2l-1-0.3c0.2-0.7,0.3-1.4,0.4-2.1
          		c0.1-0.7,0.1-1.4,0.1-2.1c0-4.2-1.7-7.9-4.4-10.6c-2.7-2.7-6.5-4.4-10.6-4.4c-0.6,0-1.2,0-1.7,0.1c-0.6,0.1-1.1,0.2-1.7,0.3
          		L12.6,0.8z"></path>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#C5C6C6" d="M20.4,32.2c-0.7,0.2-1.4,0.3-2.1,0.4c-0.7,0.1-1.4,0.1-2.1,0.1
          		c-4.4,0-8.5-1.8-11.4-4.7C2,25.1,0.2,21.1,0.2,16.6c0-0.8,0.1-1.5,0.2-2.3c0.1-0.7,0.3-1.5,0.5-2.2l1,0.3c-0.2,0.7-0.3,1.4-0.4,2.1
          		c-0.1,0.7-0.1,1.4-0.1,2.1c0,4.2,1.7,7.9,4.4,10.6c2.7,2.7,6.5,4.4,10.6,4.4c0.7,0,1.3,0,2-0.1c0.6-0.1,1.3-0.2,1.9-0.4L20.4,32.2z
          		"></path>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#C5C6C6" d="M0,10.4C0.9,8,2.3,5.8,4.2,4c1.8-1.8,4-3.2,6.5-4l1.1,3.3
          		C9.8,3.9,8,5,6.6,6.5C5.1,7.9,4,9.6,3.2,11.6L0,10.4z"></path>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#C5C6C6" d="M32.5,22.5c-0.9,2.4-2.3,4.6-4.2,6.4c-1.8,1.8-4,3.2-6.5,4
          		l-1.1-3.3c2-0.7,3.8-1.8,5.2-3.2c1.5-1.4,2.6-3.2,3.3-5.1L32.5,22.5z"></path>
        </g>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#C5C6C6" stroke-width="0.4799" stroke-miterlimit="22.9256" d="
          	M47.8,24.6h1.9V9.4h-3.8l-0.9,6l-0.5,5.3h-0.1l-0.5-5.4l-0.9-6H38v1.8h1.9v0.1l-0.8,0.4v12.8H41V11.2h0.3l2,12h2.4l1.9-12h0.3V24.6
          	L47.8,24.6z M54.3,16.2v-4.9h2.6V13l1.9,0V9.4h-7.6v1.8h1.9v0.1l-0.8,0.5v12.8H59v-3.4h-1.9v1.6h-2.8V18h3.6v-1.8H54.3L54.3,16.2z
          	 M63.4,22.7V11.3h2.3l1.2,1.2v9.1l-1.2,1.2H63.4L63.4,22.7z M62.3,11.3v0.1l-0.8,0.5v12.7h5l2.4-2.3V11.7l-2.4-2.3h-6.2v1.8H62.3
          	L62.3,11.3z M75.8,24.6v-1.7h-1.7V11.1h1.5V9.4h-4.9v1.7H73v0.1l-0.8,0.5v11.2h-1.7v1.7H75.8L75.8,24.6z M82.5,18.8h-2.7l1.2-7.6
          	h0.4L82.5,18.8L82.5,18.8z M78,11.2h1.6v0.1l-0.8,0.5l-1.9,12.8h1.9l0.6-4h3.3l0.6,4h1.9L83,9.4H78V11.2z"></path>
          <g>
            <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="12.1,21 12.1,20 11.1,20 11.1,13.1 12,13.1 12,12.1
          		9.2,12.1 9.2,13.1 10.5,13.1 10.5,13.2 10.1,13.5 10.1,20 9.1,20 9.1,21 	"></polygon>
            <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="18.2,21 19.3,21 19.3,12.1 17.1,12.1 16.6,15.7
          		16.3,18.8 16.3,18.8 16,15.6 15.5,12.1 12.7,12.1 12.7,13.2 13.8,13.2 13.8,13.3 13.3,13.5 13.3,21 14.4,21 14.4,13.2 14.5,13.2
          		15.7,20.2 17,20.2 18.1,13.2 18.2,13.2 	"></polygon>
            <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="21.1,18.1 20,18.1 20,19.7 21.3,21.1 23,21.1 24.3,19.7
          		24.3,17.3 21.2,15.1 21.2,13.8 21.8,13.2 22.6,13.2 23.2,13.8 23.2,14.8 24.3,14.8 24.3,13.4 23,12.1 21.4,12.1 20.1,13.4
          		20.1,15.6 23.3,17.9 23.3,19.3 22.6,20 21.8,20 21.1,19.3 	"></polygon>
          </g>
          <text transform="matrix(1 0 0 1 35.232 32.4448)">
            <tspan x="3.3" y="0" fill="#C5C6C6" font-size="4.7299">маркетинговое агенство</tspan>
          </text>
      </svg>
      </a>
      <span class="footer__copywrite">© 2016</span>
    </footer>
  </section>



  <div id="delux10" class="popup_descr">
    <p class="popup_descr_tittle">Технические характеристики твердотопливного котла <br> Буржуй<sup>ТМ</sup> Deluxe-10</p>
    <table>
      <tbody><tr><td>Номинальная мощность, кВт </td><td>10</td>
      </tr>
      <tr>
        <td>Отапливаемая площадь до м2 </td><td>100</td>
      </tr>
      <tr>
        <td>Объем теплоносителя (воды) в котле, л.</td><td>61</td>
      </tr>
      <tr>
        <td>Объем камеры сгорания, дм3</td><td>56</td>
      </tr>
      <tr>
        <td>КПД при работе в отопительном режиме, % не менее </td><td>76</td>
      </tr>
      <tr>
        <td>Макс. потребление электричества</td><td>100Вт.</td>
      </tr>
      <tr>
        <td>Максимальное рабочее давление, МПа </td><td>0,3 МПа</td>
      </tr>
      <tr>
        <td>Температура нагрева воды °С </td><td>max 95 °C</td>
      </tr>
      <tr>
        <td>Наружные размеры дымохода, мм </td><td>114</td>
      </tr>
      <tr>
        <td>Температура продуктов сгорания °С</td><td>140-400</td>
      </tr>
      <tr>
        <td>Минимальная высота дымохода, м</td><td>5</td>
      </tr>
      <tr>
        <td>Диаметр входного и выходного патрубков, дюйма </td><td>G1 1/2</td>
      </tr>
      <tr>
        <td>Габаритные размеры, мм:</td><td></td>
      </tr>
      <tr>
        <td>Ширина </td><td>555</td>
      </tr>
      <tr>
        <td>Глубина</td><td>805</td>
      </tr>
      <tr>
        <td>Высота </td><td>1315</td>
      </tr>
      <tr>
        <td>Масса кг, не более </td><td>180</td></tr><tr>
      </tr>
      </tbody></table>
  </div>

<div id="delux14" class="popup_descr">
  <p class="popup_descr_tittle">Технические характеристики твердотопливного котла <br> Буржуй<sup>ТМ</sup> Deluxe-14</p>
  <table>
    <tbody><tr><td>Номинальная мощность, кВт </td><td>14</td>
    </tr>
    <tr>
      <td>Отапливаемая площадь до м2 </td><td>140</td>
    </tr>
    <tr>
      <td>Объем теплоносителя (воды) в котле, л.</td><td>66</td>
    </tr>
    <tr>
      <td>Объем камеры сгорания, дм3</td><td>67</td>
    </tr>
    <tr>
      <td>КПД при работе в отопительном режиме, % не менее </td><td>76</td>
    </tr>
    <tr>
      <td>Макс. потребление электричества</td><td>100Вт.</td>
    </tr>
    <tr>
      <td>Максимальное рабочее давление, МПа </td><td>0,3 МПа</td>
    </tr>
    <tr>
      <td>Температура нагрева воды °С </td><td>max 95 °C</td>
    </tr>
    <tr>
      <td>Наружные размеры дымохода, мм </td><td>140</td>
    </tr>
    <tr>
      <td>Температура продуктов сгорания °С</td><td>140-400</td>
    </tr>
    <tr>
      <td>Минимальная высота дымохода, м</td><td>5</td>
    </tr>
    <tr>
      <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2</td>
    </tr>
    <tr>
      <td>Габаритные размеры, мм:</td><td></td>
    </tr>
    <tr>
      <td>Ширина </td><td>555</td>
    </tr>
    <tr>
      <td>Глубина</td><td>805</td>
    </tr>
    <tr>
      <td>Высота </td><td>1385</td>
    </tr>
    <tr>
      <td>Масса кг, не более </td><td>245</td></tr><tr>
    </tr>
    </tbody></table>
</div>

<div id="delux18" class="popup_descr">
  <p class="popup_descr_tittle">Технические характеристики твердотопливного котла <br> Буржуй<sup>ТМ</sup> Deluxe-18</p>
  <table>
    <tbody><tr><td>Номинальная мощность, кВт </td><td>18</td>
    </tr>
    <tr>
      <td>Отапливаемая площадь до м2 </td><td>180</td>
    </tr>
    <tr>
      <td>Объем теплоносителя (воды) в котле, л.</td><td>74</td>
    </tr>
    <tr>
      <td>Объем камеры сгорания, дм3</td><td>77</td>
    </tr>
    <tr>
      <td>КПД при работе в отопительном режиме, % не менее </td><td>76</td>
    </tr>
    <tr>
      <td>Макс. потребление электричества</td><td>100Вт.</td>
    </tr>
    <tr>
      <td>Максимальное рабочее давление, МПа </td><td>0,3 МПа</td>
    </tr>
    <tr>
      <td>Температура нагрева воды °С </td><td>max 95 °C</td>
    </tr>
    <tr>
      <td>Наружные размеры дымохода, мм </td><td>140</td>
    </tr>
    <tr>
      <td>Температура продуктов сгорания °С</td><td>140-400</td>
    </tr>
    <tr>
      <td>Минимальная высота дымохода, м</td><td>6</td>
    </tr>
    <tr>
      <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2</td>
    </tr>
    <tr>
      <td>Габаритные размеры, мм:</td><td></td>
    </tr>
    <tr>
      <td>Ширина </td><td>555</td>
    </tr>
    <tr>
      <td>Глубина</td><td>855</td>
    </tr>
    <tr>
      <td>Высота </td><td>1385</td>
    </tr>
    <tr>
      <td>Масса кг, не более </td><td>254</td></tr><tr>
    </tr>
    </tbody></table>
</div>

<div id="delux24" class="popup_descr">
  <p class="popup_descr_tittle">Технические характеристики твердотопливного котла <br> Буржуй<sup>ТМ</sup> Deluxe-20</p>
  <table>
    <tbody><tr><td>Номинальная мощность, кВт </td><td>24</td>
    </tr>
    <tr>
      <td>Отапливаемая площадь до м2 </td><td>240</td>
    </tr>
    <tr>
      <td>Объем теплоносителя (воды) в котле, л.</td><td>84</td>
    </tr>
    <tr>
      <td>Объем камеры сгорания, дм3</td><td>100</td>
    </tr>
    <tr>
      <td>КПД при работе в отопительном режиме, % не менее </td><td>76</td>
    </tr>
    <tr>
      <td>Макс. потребление электричества</td><td>100Вт.</td>
    </tr>
    <tr>
      <td>Максимальное рабочее давление, МПа </td><td>0,3 МПа</td>
    </tr>
    <tr>
      <td>Температура нагрева воды °С </td><td>max 95 °C</td>
    </tr>
    <tr>
      <td>Наружные размеры дымохода, мм </td><td>165</td>
    </tr>
    <tr>
      <td>Температура продуктов сгорания °С</td><td>140-400</td>
    </tr>
    <tr>
      <td>Минимальная высота дымохода, м</td><td>6</td>
    </tr>
    <tr>
      <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2</td>
    </tr>
    <tr>
      <td>Габаритные размеры, мм:</td><td></td>
    </tr>
    <tr>
      <td>Ширина </td><td>555</td>
    </tr>
    <tr>
      <td>Глубина</td><td>985</td>
    </tr>
    <tr>
      <td>Высота </td><td>1445</td>
    </tr>
    <tr>
      <td>Масса кг, не более </td><td>291</td></tr><tr>
    </tr>
    </tbody></table>
</div>



  <div class="modal callback-modal" id="callback-modal" data-type="modal">
    <div class="modal__wrapper modal-wrapper">
      <span class="modal__close modal-close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Оставьте заявку сейчас
      </span>
      <span class="modal__subtitle">
        и менеджер свяжется с вами в течении 5 минут
      </span>
        <form class="formGo" action="sendmessage.php" method="POST" >
          <div class="modal__input-wrapper">
            <label class="modal__label" for="callback-modal-text-1">Введите номер телефона</label>
            <input type="text" id="callback-modal-text-1" class="modal__input" name="phone" placeholder="+38 (___) ___-__-__">
          </div>
          <div class="modal__input-wrapper">
            <input type="text"  class="modal__input" placeholder="Введите Ваше имя" name="name">
          </div>
          <input type="hidden" value="" name="order">
          <input type="hidden" value="" name="htmlData">
          <div class="modal__input-wrapper">
            <button  type="submit" class="modal__button" data-content="Получить скидку"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal callback-modal" id="callback-modal2" data-type="modal">
    <div class="modal__wrapper modal-wrapper">
      <span class="modal__close modal-close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Оставьте заявку
      </span>
      <span class="modal__subtitle">
        и менеджер свяжется с вами в течении 5 минут
      </span>
        <form class="formGo" action="sendmessage.php" method="POST" >
          <div class="modal__input-wrapper">
            <label class="modal__label" for="callback-modal-text-2">Введите номер телефона</label>
            <input type="text" id="callback-modal-text-2" class="modal__input" name="phone" placeholder="+38 (___) ___-__-__">
          </div>
          <div class="modal__input-wrapper">
            <input type="text" class="modal__input" placeholder="Введите Ваше имя" name="name">
          </div>
          <input type="hidden" name="htmlData">
          <input type="hidden" value="" name="order">
          <div class="modal__input-wrapper">
            <button type="submit" class="modal__button" data-content="Оставить заявку"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal response-modal" data-type="modal" id="response-modal">
    <div class="modal__wrapper">
      <span class="modal__close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Сообщение отправлено!<br>
        Мы свяжемся с Вами в ближайшее время.
      </span>
      </div>
    </div>
  </div>

  <script src="js/common.js"></script>

<!-- Yandex.Metrika counter -->
<!--<script type="text/javascript">-->
<!--  (function (d, w, c) {-->
<!--    (w[c] = w[c] || []).push(function() {-->
<!--      try {-->
<!--        w.yaCounter36218195 = new Ya.Metrika({-->
<!--          id:36218195,-->
<!--          clickmap:true,-->
<!--          trackLinks:true,-->
<!--          accurateTrackBounce:true,-->
<!--          webvisor:true,-->
<!--          ecommerce:"dataLayer"-->
<!--        });-->
<!--      } catch(e) { }-->
<!--    });-->
<!---->
<!--    var n = d.getElementsByTagName("script")[0],-->
<!--        s = d.createElement("script"),-->
<!--        f = function () { n.parentNode.insertBefore(s, n); };-->
<!--    s.type = "text/javascript";-->
<!--    s.async = true;-->
<!--    s.src = "https://mc.yandex.ru/metrika/watch.js";-->
<!---->
<!--    if (w.opera == "[object Opera]") {-->
<!--      d.addEventListener("DOMContentLoaded", f, false);-->
<!--    } else { f(); }-->
<!--  })(document, window, "yandex_metrika_callbacks");-->
<!--</script>-->
<!--<noscript><div><img src="https://mc.yandex.ru/watch/36218195" style="position:absolute; left:-9999px;" alt="" /></div></noscript>-->
  <!-- /Yandex.Metrika counter -->
<!--  <script>-->
<!--    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){-->
<!--              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),-->
<!--            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)-->
<!--    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');-->
<!---->
<!--    ga('create', 'UA-74413080-1', 'auto');-->
<!--    ga('send', 'pageview');-->
<!---->
<!--  </script>-->

  
  <!— BEGIN JIVOSITE CODE {literal} —>
  <script type='text/javascript'>
    (function(){ var widget_id = 'K66kAqWI7Q';
      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
  <!— {/literal} END JIVOSITE CODE —>


<!--  <noindex>-->
<!--    <link rel="stylesheet" href="//cabinet.salesupwidget.com/widget/tracker.css">-->
<!--    <script type="text/javascript" src="//cabinet.salesupwidget.com/php/1.js" charset="UTF-8" async></script >-->
<!--    <script type="text/javascript">var uid_code="1019";</script>-->
<!--  </noindex>-->
</body>

</html>
